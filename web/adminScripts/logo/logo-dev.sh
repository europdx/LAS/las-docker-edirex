#!/usr/bin/env bash

WORK_DIR=/srv
LOGO_FILE=logo_source_dev.png
TARGET_FILES=("logo.png" "logo-arancio.png" "logo-trasp.png" "logo-verde.png" "logo.PNG")

for TARGET_FILE in ${TARGET_FILES[*]}
do
  echo "Substitute $TARGET_FILE with $LOGO_FILE."
  find $WORK_DIR -name "$TARGET_FILE" -exec bash -c "cp $LOGO_FILE {}" \;
done
