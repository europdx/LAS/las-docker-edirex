#/bin/sh

if [ ! $HOST = "lasleuven.ics.muni.cz" ]
then
  /virtualenvs/venvdj1.4/bin/python /srv/www/LASAuthServer/set_admin_password.py
fi
